package com.cnova.api.v2.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "")
public class GetOrdersTrackingUpdatingResponse  {
  
  private List<OrderTrackingUpdatingStatus> trackings = new ArrayList<OrderTrackingUpdatingStatus>() ;
  private List<MetadataEntry> metadata = new ArrayList<MetadataEntry>() ;

  
  /**
   * Informações de tracking do pedido
   **/
  @ApiModelProperty(value = "Informações de tracking do pedido")
  @JsonProperty("trackings")
  public List<OrderTrackingUpdatingStatus> getTrackings() {
    return trackings;
  }
  public void setTrackings(List<OrderTrackingUpdatingStatus> trackings) {
    this.trackings = trackings;
  }

  
  /**
   * Dados adicionais da consulta
   **/
  @ApiModelProperty(value = "Dados adicionais da consulta")
  @JsonProperty("metadata")
  public List<MetadataEntry> getMetadata() {
    return metadata;
  }
  public void setMetadata(List<MetadataEntry> metadata) {
    this.metadata = metadata;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetOrdersTrackingUpdatingResponse {\n");
    
    sb.append("  trackings: ").append(trackings).append("\n");
    sb.append("  metadata: ").append(metadata).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

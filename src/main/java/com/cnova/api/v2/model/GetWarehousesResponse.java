package com.cnova.api.v2.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "")
public class GetWarehousesResponse  {
  
  private List<Warehouse> warehouses = new ArrayList<Warehouse>() ;
  private List<MetadataEntry> metadata = new ArrayList<MetadataEntry>() ;

  
  /**
   **/
  @ApiModelProperty(required = true, value = "")
  @JsonProperty("warehouses")
  public List<Warehouse> getWarehouses() {
    return warehouses;
  }
  public void setWarehouses(List<Warehouse> warehouses) {
    this.warehouses = warehouses;
  }

  
  /**
   **/
  @ApiModelProperty(required = true, value = "")
  @JsonProperty("metadata")
  public List<MetadataEntry> getMetadata() {
    return metadata;
  }
  public void setMetadata(List<MetadataEntry> metadata) {
    this.metadata = metadata;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetWarehousesResponse {\n");
    
    sb.append("  warehouses: ").append(warehouses).append("\n");
    sb.append("  metadata: ").append(metadata).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

package com.cnova.api.v2.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "")
public class SellerItemStocks  {
  
  private String skuSellerId = null;
  private List<Stock> stocks = new ArrayList<Stock>() ;

  
  /**
   * SKU do produto do lojista que deverá ser alterado
   **/
  @ApiModelProperty(required = true, value = "SKU do produto do lojista que deverá ser alterado")
  @JsonProperty("skuSellerId")
  public String getSkuSellerId() {
    return skuSellerId;
  }
  public void setSkuSellerId(String skuSellerId) {
    this.skuSellerId = skuSellerId;
  }

  
  /**
   * Estoque do produto
   **/
  @ApiModelProperty(required = true, value = "Estoque do produto")
  @JsonProperty("stocks")
  public List<Stock> getStocks() {
    return stocks;
  }
  public void setStocks(List<Stock> stocks) {
    this.stocks = stocks;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class SellerItemStocks {\n");
    
    sb.append("  skuSellerId: ").append(skuSellerId).append("\n");
    sb.append("  stocks: ").append(stocks).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

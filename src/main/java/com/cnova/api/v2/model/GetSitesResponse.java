package com.cnova.api.v2.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "")
public class GetSitesResponse  {
  
  private Integer totalRows = null;
  private List<Site> sites = new ArrayList<Site>() ;

  
  /**
   **/
  @ApiModelProperty(required = true, value = "")
  @JsonProperty("totalRows")
  public Integer getTotalRows() {
    return totalRows;
  }
  public void setTotalRows(Integer totalRows) {
    this.totalRows = totalRows;
  }

  
  /**
   **/
  @ApiModelProperty(required = true, value = "")
  @JsonProperty("sites")
  public List<Site> getSites() {
    return sites;
  }
  public void setSites(List<Site> sites) {
    this.sites = sites;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetSitesResponse {\n");
    
    sb.append("  totalRows: ").append(totalRows).append("\n");
    sb.append("  sites: ").append(sites).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}

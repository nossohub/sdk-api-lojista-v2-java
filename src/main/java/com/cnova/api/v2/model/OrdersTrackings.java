package com.cnova.api.v2.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(description = "")
public class OrdersTrackings  {
  
  private List<OrderTracking> trackings = new ArrayList<OrderTracking>() ;

  
  /**
   * Lista de trackings
   **/
  @ApiModelProperty(value = "Lista de trackings")
  @JsonProperty("trackings")
  public List<OrderTracking> getTrackings() {
    return trackings;
  }
  public void setTrackings(List<OrderTracking> trackings) {
    this.trackings = trackings;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class OrdersTrackings {\n");
    
    sb.append("  trackings: ").append(trackings).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
